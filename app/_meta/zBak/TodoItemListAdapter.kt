package com.dev.demo2019.model.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dev.commons.general.event.ItemClickListener
import com.dev.commons.general.util.Logr
import com.dev.demo2019.R
import com.dev.demo2019.databinding.RowTodoItemBinding
import com.dev.demo2019.fragment_aac.viewmodel_aac.ItemsListViewModel
import com.dev.demo2019.model.pojo.TodoItem

/*
 * reference:
 * https://www.raywenderlich.com/1560485-android-recyclerview-tutorial-with-kotlin
 */
class TodoItemListAdapter(private val viewModel: ItemsListViewModel) : RecyclerView.Adapter<TodoItemListAdapter.TodoItemViewHolder>() {
    private var mTodoItems = emptyList<TodoItem>()
    private val mInflater: LayoutInflater = LayoutInflater.from(viewModel.getApplication())
    private var mClickListener: ItemClickListener? = null

    fun setTodoItems(todoItems: List<TodoItem>) {
        mTodoItems = todoItems
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoItemViewHolder {
        val binding = DataBindingUtil.inflate<RowTodoItemBinding>(
                mInflater,
                R.layout.row_todo_item,
                parent,
                false)

        return TodoItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TodoItemViewHolder, position: Int) {
        holder.binding.todoItem = viewModel.getTodoViewItem(position)
                .also { Logr.debug("list adapter loads todoItemViewModel: $it") }
    }

    fun setClickListener(itemClickListener: ItemClickListener) {
        mClickListener = itemClickListener
    }

    override fun getItemCount() = mTodoItems.size

    inner class TodoItemViewHolder(val binding: RowTodoItemBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        init {
            binding.root.setOnClickListener(this)
            binding.executePendingBindings()
        }

        override fun onClick(view: View) {
            mClickListener?.onItemClick(view, adapterPosition)
        }

    }

}
