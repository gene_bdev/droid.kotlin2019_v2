package com.dev.default_kt_module_pkg

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dev.demo2019.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
