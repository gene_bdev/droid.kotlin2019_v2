package com.dev.demo2019.model.repository

import com.dev.commons.general.util.Logr
import com.dev.demo2019.model.pojo.TodoItem
import com.dev.demo2019.platform.retrofit.AppRetrofitNetworkService
import com.dev.demo2019.platform.retrofit.model.error.ErrorResponse
import com.dev.demo2019.platform.retrofit.model.error.RetrofitResponseException
import com.google.gson.Gson
import org.json.JSONObject
import retrofit2.Response

/**
 * Repository pattern
 */
class TodoItemsRepository(
        private val mAppState: AppState,
        private val appRetrofitNetworkService: AppRetrofitNetworkService
) {

    fun loadTodoItems() =
            appRetrofitNetworkService
                    .api
                    .todoItemsRx
                    .map { mapToPayload(it) }
                    .doOnSuccess { mAppState.todoItems = it }  // Save loaded items to AppState
                    .doOnError {

                        Logr.debug("Throwable: " + it.message)
                        it.printStackTrace()
                    }

    private fun mapToPayload(response: Response<List<TodoItem>>): List<TodoItem> {
        if (!response.isSuccessful) {
            throw RetrofitResponseException(getErrorResponseBody(response))
        }

        return response.body() ?: throw RetrofitResponseException()
    }

    private fun getErrorResponseBody(response: Response<*>): ErrorResponse? {

        val errorBody = response.errorBody()

        var errorModel: ErrorResponse? = null

        try {
            val errorBodyStr = errorBody!!.string()
            val jObjError = JSONObject(errorBodyStr)
            errorModel = Gson().fromJson(jObjError.toString(), ErrorResponse::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return errorModel
    }

    fun getCachedTodoItem(position: Int) = mAppState.getTodoItem(position)

    fun getCachedTodoItems() = mAppState.todoItems

}

