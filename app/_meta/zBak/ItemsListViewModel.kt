package com.dev.demo2019.fragment_aac.viewmodel_aac

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dev.demo2019.model.mapper.AppMapper
import com.dev.demo2019.model.pojo.TodoItem
import com.dev.demo2019.model.repository.TodoItemsRepository
import com.dev.demo2019.platform.TheApp
import javax.inject.Inject

class ItemsListViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var mRepository: TodoItemsRepository
        @Inject set

    lateinit var mAppMapper: AppMapper
        @Inject set

    private val _itemsLiveData = MutableLiveData<List<TodoItem>>()

    fun getItemsLiveData() = _itemsLiveData as LiveData<List<TodoItem>>

    init {
        TheApp.graph().inject(this)

        _itemsLiveData.value = mRepository.getCachedTodoItems()
    }

    fun getTodoViewItem(position: Int) =
            mAppMapper.getViewItem(
                    mRepository.getCachedTodoItem(position)
            )

}
