package com.dev.demo2019.fragment_aac.viewmodel_aac

import androidx.lifecycle.ViewModel
import com.dev.demo2019.model.mapper.AppMapper
import com.dev.demo2019.model.repository.TodoItemsRepository
import com.dev.demo2019.platform.TheApp
import javax.inject.Inject

class ItemDetailsViewModel : ViewModel() {

    lateinit var mRepository: TodoItemsRepository
        @Inject set

    lateinit var mAppMapper: AppMapper
        @Inject set

    init {
        TheApp.graph().inject(this)
    }

    fun getTodoViewItem(position: Int) =
            mAppMapper.getViewItem(
                    mRepository.getCachedTodoItem(position)
            )

}
