package com.dev.demo2019.platform

import android.app.Application
import com.dev.demo2019.di.AppModule
import com.dev.demo2019.dagger_di.AppGraph
import com.dev.demo2019.dagger_di.DaggerAppGraph


class TheApp : Application() {

    override fun onCreate() {
        super.onCreate()

        sAppGraph = DaggerAppGraph
                .builder()
                .appModule(AppModule(this))
                .build()
    }

    companion object {
        private lateinit var sAppGraph: AppGraph
        fun graph() = sAppGraph
    }

}
