package com.dev.demo2019.platform.retrofit

import com.google.gson.GsonBuilder

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class AppRetrofitNetworkService(baseUrl: String) {

    val api: AppRetrofitEndpoints

    init {
        val gson = GsonBuilder()
                .setLenient()
                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(buildHttpClient())
                .build()

        api = retrofit.create(AppRetrofitEndpoints::class.java)
    }

    private fun buildHttpClient(): OkHttpClient {

        val builder = OkHttpClient.Builder()

        // set access for logging, for debug only
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BASIC
        logging.level = HttpLoggingInterceptor.Level.BODY
        logging.level = HttpLoggingInterceptor.Level.HEADERS

        builder.addInterceptor(logging)

        builder.addInterceptor { chain ->
            val request: Request = chain.request().newBuilder()
                    .addHeader("Accept", "application/json")
                    .build()

            chain.proceed(request)
        }

        return builder.build()
    }
}
