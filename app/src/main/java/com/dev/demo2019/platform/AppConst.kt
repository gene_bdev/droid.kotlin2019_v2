package com.dev.demo2019.platform

/*
https://kotlinlang.org/docs/tutorials/kotlin-for-py/objects-and-companion-objects.html
If you need a singleton - a class that only has got one instance
- you can declare the class in the usual way, but use the object keyword instead of class:

There will only ever be one instance of this class, and the instance
(which is created the first time it is accessed, in a thread-safe manner)
has got the same name as the class
 */
object AppConst {

    object Field {
        const val id = "id"
        const val title = "title"
        const val completed = "completed"


        const val storeID = "storeID"
        const val name = "name"

        const val phone = "phone"
        const val storeLogoURL = "storeLogoURL"

        const val address = "address"
        const val city = "city"
        const val zipcode = "zipcode"
        const val state = "state"
        const val latitude = "latitude"
        const val longitude = "longitude"
    }

    object Extra {
        const val LIST_INDEX = "list_index"
    }

}
