package com.dev.demo2019.platform.retrofit.model.error

class RetrofitResponseException(val error: ErrorResponse? = null) : RuntimeException()
