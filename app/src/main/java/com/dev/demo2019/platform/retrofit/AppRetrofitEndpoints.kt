package com.dev.demo2019.platform.retrofit

import com.dev.demo2019.model.pojo.StoresContainer
import com.dev.demo2019.model.pojo.TodoItem
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface AppRetrofitEndpoints {

    /**
    Was running into error with just @GET("/todos")

    Found this solution to apply @get annotation

    Sources include:
    1) https://kotlinlang.org/docs/reference/annotations.html#java-annotations
    // apply @Rule annotation to property getter
    @get:Rule val tempFolder = TemporaryFolder()

     2) https://www.baeldung.com/kotlin-annotations
    In the case of placing @get:Positive on a Kotlin field,
    it would mean that the annotation should actually target the generated getter for that field.
     */

     @get:GET("/todos")
     val todoItemsRx: Single<Response<List<TodoItem>>>

    @get:GET("/BR_Android_CodingExam_2015_Server/stores.json")
    val storeLocationsRx: Single<Response<StoresContainer>>

}
