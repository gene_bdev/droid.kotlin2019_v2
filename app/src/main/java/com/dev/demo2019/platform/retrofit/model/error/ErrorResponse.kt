package com.dev.demo2019.platform.retrofit.model.error

class ErrorResponse {

    var isOk: Boolean = false
    var error: Error? = null

    inner class Error {
        var code: Int = 0
        var reason: String? = null
    }
}
