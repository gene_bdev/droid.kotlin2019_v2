package com.dev.demo2019.platform.retrofit.model.error

import android.app.AlertDialog
import android.content.Context
import android.widget.Toast

import java.io.IOException

object AppRetrofitError {

    fun handleError(context: Context, errObj: Any) {

        when (errObj) {
            is RetrofitResponseException -> errObj.error?.let { errHandleDialog(context, it) }
            is String -> errHandleDialog(context, errObj)
            is IOException -> {
                Toast.makeText(context, "No internet connection available!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun errHandleDialog(context: Context, errorMsg: String) {
        val alert = AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(errorMsg)
                .setPositiveButton("Ok", null).create()

        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

    private fun errHandleDialog(context: Context, errorResponse: ErrorResponse) {

        /*
        Using !!. for null check
            > from https://kotlinlang.org/docs/reference/null-safety.html#the--operator
        not-null assertion operator (!!) converts any value to a non-null type
        and throws an exception if the value is null. We can write b!!,
        and this will return a non-null value of b (e.g., a String in our example)
        or throw an NPE if b is null:
            `val l = b!!.length`
         */
        val alert = AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(errorResponse.error!!.reason)
                .setPositiveButton("Ok", null).create()

        alert.setCanceledOnTouchOutside(false)
        alert.show()

        val btnOkay = alert.getButton(AlertDialog.BUTTON_POSITIVE)

        btnOkay.setOnClickListener {
            when (errorResponse.error!!.code) {

                1001 -> {
                }

                1002 -> {
                }

                1003 -> {
                }
            }
            alert.dismiss()
        }
    }

}
