package com.dev.demo2019.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dev.demo2019.R
import com.dev.demo2019.databinding.ActivityStartBinding
import com.dev.demo2019.fragment_aac.viewmodel_aac.StartActivityViewModel
import com.dev.demo2019.platform.retrofit.model.error.AppRetrofitError

/**
 * Reference:
 * https://proandroiddev.com/architecture-components-modelview-livedata-33d20bdcc4e9
 */
class StartActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityStartBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle(
                getString(R.string.app_name)
        )
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_start)

        initializeViewModel()

        setListeners()
    }

    private fun initializeViewModel() {

        // operator `::`
        // creates a member reference or a class reference, from https://kotlinlang.org/docs/reference/keyword-reference.html

        // AAC ViewModel & ViewModelProviders, from https://medium.com/the-lair/internals-of-android-architecture-components-part-i-the-viewmodel-d893e362a0d9

        // 1. StartActivityViewModel constructor/init-block calls: loadStoreLocations()
        val viewModel =
            ViewModelProviders
                .of(this)
                .get(StartActivityViewModel::class.java)

        // 2. these are the success/error listener-callbacks for /loadStoreLocations()

        viewModel
            .getIsLoadingLD()
            .observe(this, Observer(::onLoadingStateChanged))

        viewModel
            .getErrorLD()
            .observe(this, Observer(::handleError))
    }

    private fun onLoadingStateChanged(isLoading: Boolean) {

        if (isLoading) {

            mBinding.progressBar.visibility = View.VISIBLE
            mBinding.btnGo.visibility = View.GONE
        }
        else {
            mBinding.progressBar.visibility = View.GONE
            mBinding.btnGo.visibility = View.VISIBLE
        }
    }

    private fun handleError(error: Throwable) {
        AppRetrofitError.handleError(this, error)
    }

    private fun setListeners() {

        mBinding
            .btnGo
            .setOnClickListener { navigateToFragmentContainerActivity() }
    }

    private fun navigateToFragmentContainerActivity() {

        val intent = Intent(this, FragmentContainerActivity::class.java)
        startActivity(intent)

        finish()
    }

}
