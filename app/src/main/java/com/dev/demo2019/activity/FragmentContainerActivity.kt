package com.dev.demo2019.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.dev.commons.core.fragment_accessor.FragmentBackStackHandler
import com.dev.commons.core.fragment_accessor.RawFragmentAccessor
import com.dev.demo2019.R
import com.dev.demo2019.databinding.ActivityFragmentContainerBinding
import com.dev.demo2019.fragment_aac.fragment.StoreLocationsListFragment

class FragmentContainerActivity : AppCompatActivity() {

    private lateinit var mFragmentBackStackHandler: FragmentBackStackHandler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<ActivityFragmentContainerBinding>(this, R.layout.activity_fragment_container)

        mFragmentBackStackHandler = FragmentBackStackHandler(this@FragmentContainerActivity)

        mFragmentBackStackHandler.isDoubleBackPressEnabled = true

        RawFragmentAccessor
            .setFragmentContainer(this, R.id.main_app_fragment_container)

        RawFragmentAccessor
                .getInstance()
                .addFragment(StoreLocationsListFragment.newInstance())
                // .addFragment(ItemsListFragment.newInstance())
    }


    override fun onBackPressed() {

        // let the fragment listener handle all onBackPress logic if it can
        val isHandled = mFragmentBackStackHandler
            .onBackPressed(
                    RawFragmentAccessor.getInstance()
            )

        if (!isHandled) {

            super.onBackPressed()  // this exits the app.
        }
    }

}
