package com.dev.demo2019.model.pojo

import com.google.gson.annotations.SerializedName

data class StoresContainer(
        @SerializedName("stores")
        val stores: List<StoreLocation>
)
