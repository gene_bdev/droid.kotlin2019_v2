package com.dev.demo2019.model.viewmodel_pojo

import com.dev.demo2019.platform.AppConst
import com.google.gson.annotations.SerializedName

data class StoreLocationViewModel(

        @SerializedName(AppConst.Field.name)
        var name: String,

        @SerializedName(AppConst.Field.phone)
        val phone: String,

        @SerializedName(AppConst.Field.address)
        val address: String,

        @SerializedName(AppConst.Field.city)
        val city: String,

        @SerializedName(AppConst.Field.zipcode)
        val zipcode: String,

        @SerializedName(AppConst.Field.state)
        val state: String
)

