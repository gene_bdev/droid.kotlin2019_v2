package com.dev.demo2019.model.viewmodel_pojo

import com.dev.demo2019.platform.AppConst
import com.google.gson.annotations.SerializedName

data class TodoItemViewModel(

        @SerializedName(AppConst.Field.title)
        var title: String,

        @SerializedName(AppConst.Field.completed)
        var completedStatus: String
)

