package com.dev.demo2019.model.mapper

import android.content.Context
import com.dev.demo2019.R
import com.dev.demo2019.model.pojo.StoreLocation
import com.dev.demo2019.model.pojo.TodoItem
import com.dev.demo2019.model.viewmodel_pojo.StoreLocationViewModel
import com.dev.demo2019.model.viewmodel_pojo.TodoItemViewModel

class AppMapper(private val context: Context) {

    fun getViewItem(todoItem: TodoItem): TodoItemViewModel {

        val statusStringResId = if (todoItem.isCompleted)
            R.string.completed_status_true
        else
            R.string.completed_status_false

        val completedStatus = context.getString(statusStringResId)

        return TodoItemViewModel(todoItem.title, completedStatus)
    }

    fun getViewModel(storeLocation: StoreLocation): StoreLocationViewModel {

        return StoreLocationViewModel(
                storeLocation.name,
                storeLocation.phone,
                storeLocation.address,
                storeLocation.city,
                storeLocation.zipcode,
                storeLocation.state
        )
    }

}
