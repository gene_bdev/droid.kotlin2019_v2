package com.dev.demo2019.model.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.dev.commons.general.event.ItemClickListener
import com.dev.commons.general.util.Logr
import com.dev.demo2019.R
import com.dev.demo2019.databinding.RowStoreLocationBinding
import com.dev.demo2019.fragment_aac.viewmodel_aac.StoreLocationsListViewModel
import com.dev.demo2019.model.pojo.StoreLocation

/*
 * reference:
 * https://www.raywenderlich.com/1560485-android-recyclerview-tutorial-with-kotlin
 */
class StoreLocationListAdapter(private val viewModel: StoreLocationsListViewModel) :
        RecyclerView.Adapter<StoreLocationListAdapter.StoreLocationViewHolder>() {

    private var mStoreLocations = emptyList<StoreLocation>()
    private val mInflater: LayoutInflater = LayoutInflater.from(viewModel.getApplication())
    private var mClickListener: ItemClickListener? = null

    fun setStoreLocations(storeLocations: List<StoreLocation>) {
        mStoreLocations = storeLocations
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreLocationViewHolder {
        val binding = DataBindingUtil.inflate<RowStoreLocationBinding>(
                mInflater,
                R.layout.row_store_location,
                parent,
                false
        )

        return StoreLocationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StoreLocationViewHolder, position: Int) {
        holder.binding.storeLocation = viewModel.getStoreLocationViewItem(position)
            .also { Logr.debug("list adapter loads todoItemViewModel: $it") }
    }

    fun setClickListener(itemClickListener: ItemClickListener) {
        mClickListener = itemClickListener
    }

    override fun getItemCount() = mStoreLocations.size

    inner class StoreLocationViewHolder(val binding: RowStoreLocationBinding) :
            RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        init {
            binding.root.setOnClickListener(this)
            binding.executePendingBindings()
        }

        override fun onClick(view: View) {
            mClickListener?.onItemClick(view, adapterPosition)
        }

    }

}
