package com.dev.demo2019.model.pojo

import com.dev.demo2019.platform.AppConst
import com.google.gson.annotations.SerializedName

// idiomatic way to declare POJO in Kotlin
// from https://kotlinlang.org/docs/reference/data-classes.html
data class TodoItem(
        @SerializedName(AppConst.Field.id)
        val id: String,

        @SerializedName(AppConst.Field.title)
        val title: String,

        @SerializedName(AppConst.Field.completed)
        val isCompleted: Boolean
)
