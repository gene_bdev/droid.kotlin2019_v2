package com.dev.demo2019.model.pojo

import com.dev.demo2019.platform.AppConst
import com.google.gson.annotations.SerializedName

data class StoreLocation (

        @SerializedName(AppConst.Field.storeID)
        val storeID: String,

        @SerializedName(AppConst.Field.name)
        val name: String,

        @SerializedName(AppConst.Field.phone)
        val phone: String,

        @SerializedName(AppConst.Field.storeLogoURL)
        val storeLogoURL: String,

        @SerializedName(AppConst.Field.address)
        val address: String,

        @SerializedName(AppConst.Field.city)
        val city: String,

        @SerializedName(AppConst.Field.zipcode)
        val zipcode: String,

        @SerializedName(AppConst.Field.state)
        val state: String,

        @SerializedName(AppConst.Field.latitude)
        val latitude: Double,

        @SerializedName(AppConst.Field.longitude)
        val longitude: Double
)
