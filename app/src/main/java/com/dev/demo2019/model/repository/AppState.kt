package com.dev.demo2019.model.repository

import com.dev.demo2019.model.pojo.StoreLocation
import com.dev.demo2019.model.pojo.TodoItem

class AppState {

    var todoItems = emptyList<TodoItem>()

    var storeLocations = emptyList<StoreLocation>()

    fun getStoreLocation(index: Int) = storeLocations[index]

    fun getTodoItem(index: Int) = todoItems[index]

}
