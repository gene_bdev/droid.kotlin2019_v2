package com.dev.demo2019.fragment_aac.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.commons.core.fragment_accessor.RawFragmentAccessor
import com.dev.commons.core.fragment_accessor.app_fragment.RawBaseFragment
import com.dev.commons.general.event.ItemClickListener
import com.dev.commons.general.util.Logr
import com.dev.demo2019.R
import com.dev.demo2019.databinding.FragmentStoresListBinding
import com.dev.demo2019.fragment_aac.viewmodel_aac.StoreLocationsListViewModel
import com.dev.demo2019.model.adapter.StoreLocationListAdapter

class StoreLocationsListFragment : RawBaseFragment() {

    private lateinit var mBinding: FragmentStoresListBinding

    private val mStoreLocationListAdapter by lazy {

        /*
        Syntax: inline fun <T> T.apply(block: T.() -> Unit): T
         Calls the specified function block with this value as its receiver and returns `this` value.
         https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/apply.html
         */

        // AAC Construct:
        // Call to constructor starts with loading `val viewModel by lazy`  first time it's accessed, via ViewModelProviders
        StoreLocationListAdapter(viewModel)
            .apply {
                setClickListener(
                    /*
                    If the lambda parameter is unused, you can place an underscore instead of its name:
                    from https://kotlinlang.org/docs/reference/lambdas.html#underscore-for-unused-variables-since-11
                     */
                    ItemClickListener { _,
                                        position ->
                        val msg = "position: $position"
                        Logr.debug(msg)
                        navigateToItemDetailsFragment(position)
                    })
            }
    }

    private val viewModel by lazy {

        ViewModelProviders
            .of(this)
            .get(StoreLocationsListViewModel::class.java)
    }

    private fun navigateToItemDetailsFragment(position: Int) {

        RawFragmentAccessor
            .getInstance()
            .replaceFragment(
                    StoreDetailsFragment.newInstance(position)
            )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding = DataBindingUtil
            .inflate(
                    inflater,
                    R.layout.fragment_stores_list,
                    container,
                    false
            )

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setActionBarTitle(
                getString(R.string.stores_locations)
        )

        /*
        We recommend with for calling functions on the context object without providing the lambda result.
        In the code, with can be read as “with this object, do the following.”
        > from https://kotlinlang.org/docs/reference/scope-functions.html#with
         */
        with(mBinding.itemList) {
            layoutManager = LinearLayoutManager(context)

            adapter = mStoreLocationListAdapter
        }

        /*
        Understanding `viewLifecycleOwner`
        from https://medium.com/@BladeCoder/architecture-components-pitfalls-part-1-9300dd969808
        Thanks to the custom LifecycleOwner returned by getViewLifecycleOwner(),
        the LiveData observers will be automatically unsubscribed when the view hierarchy is destroyed and nothing else has to be done.
         */
        viewModel
            .getItemsLiveData()
            .observe(
                viewLifecycleOwner,
                Observer { mStoreLocationListAdapter.setStoreLocations(it) }
            )
    }

    // see https://kotlinlang.org/docs/tutorials/kotlin-for-py/objects-and-companion-objects.html
    companion object {
        fun newInstance() = StoreLocationsListFragment()
    }

}
