package com.dev.demo2019.fragment_aac.viewmodel_aac

import androidx.lifecycle.ViewModel
import com.dev.demo2019.model.mapper.AppMapper
import com.dev.demo2019.model.repository.StoreLocationsRepository
import com.dev.demo2019.platform.TheApp
import javax.inject.Inject

class StoreDetailsViewModel : ViewModel() {

    lateinit var mRepository: StoreLocationsRepository
        @Inject set

    lateinit var mAppMapper: AppMapper
        @Inject set

    init {
        TheApp.graph().inject(this)
    }

    fun getStoreModel(position: Int) =
            mRepository.getCachedStoreLocation(position)

    fun getStoreViewModel(position: Int) =
            mAppMapper.getViewModel(
                    mRepository.getCachedStoreLocation(position)
            )

}
