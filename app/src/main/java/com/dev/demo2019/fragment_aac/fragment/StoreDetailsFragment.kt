package com.dev.demo2019.fragment_aac.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.dev.commons.core.fragment_accessor.app_fragment.RawBaseFragment
import com.dev.demo2019.R
import com.dev.demo2019.databinding.FragmentStoreDetailsBinding
import com.dev.demo2019.fragment_aac.viewmodel_aac.StoreDetailsViewModel
import com.dev.demo2019.platform.AppConst

class StoreDetailsFragment : RawBaseFragment() {

    private lateinit var mBinding: FragmentStoreDetailsBinding

    private val viewModelAac by lazy {

        ViewModelProviders
            .of(this)
            .get(StoreDetailsViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil
            .inflate(
                    inflater,
                    R.layout.fragment_store_details,
                    container,
                    false
            )

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*
         * `it` explained: https://discuss.kotlinlang.org/t/it-keyword/6869

            When using Kotlin functional features you can do something like:

                `collection.map { println(it) }`

         */
        arguments
            ?.getInt(AppConst.Extra.LIST_INDEX)
            ?.let {


                val viewModel = viewModelAac.getStoreViewModel(it)
                mBinding.storeLocation = viewModel

                val storeModel = viewModelAac.getStoreModel(it)

                Glide.with(this)
                    .load(storeModel.storeLogoURL)
                    .into(mBinding.storeLogo)

                setActionBarTitle(
                        storeModel.name
                )


            }

    }

    companion object {
        fun newInstance(itemPosition: Int) =
            StoreDetailsFragment().apply {
                val args = Bundle()
                args.putInt(AppConst.Extra.LIST_INDEX, itemPosition)
                arguments = args
            }
    }

}
