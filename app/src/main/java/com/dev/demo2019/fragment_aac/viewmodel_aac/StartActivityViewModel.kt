package com.dev.demo2019.fragment_aac.viewmodel_aac

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dev.commons.general.network.HttpUtil
import com.dev.commons.general.util.Logr
import com.dev.demo2019.model.repository.StoreLocationsRepository
import com.dev.demo2019.platform.TheApp
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class StartActivityViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var mStoreLocationsRepository: StoreLocationsRepository
        @Inject set    // You can also annotate property accessors - from https://kotlinlang.org/docs/reference/annotations.html

    private val mDisposables = CompositeDisposable()

    /*
    LiveData is an observable data holder class. Unlike a regular observable, LiveData is lifecycle-aware, meaning it respects the lifecycle of other app components, such as activities, fragments, or services. This awareness ensures LiveData only updates app component observers that are in an active lifecycle state.
     from https://medium.com/the-lair/internals-of-android-architecture-components-part-ii-livedata-a26a4d11795

     Addt'l details on LiveData
     from https://medium.com/rocknnull/exploring-kotlin-using-android-architecture-components-and-vice-versa-aa16e600041a
     */

    //    If a class has two properties which are conceptually the same but one is part of a public API and another is an implementation detail, use an underscore as the prefix for the name of the private property:
    //    from https://kotlinlang.org/docs/reference/coding-conventions.html?_ga=2.204339024.1534855630.1573426395-1399480414.1573064249#property-names
    private val _isLoadingLD = MutableLiveData<Boolean>()

    fun getIsLoadingLD() = _isLoadingLD as LiveData<Boolean>

    private val _errorLD = MutableLiveData<Throwable>()
    fun getErrorLD() = _errorLD as LiveData<Throwable>

    // During an instance initialization, the initializer blocks are executed in the same order as they appear in the class body, interleaved with the property initializers
    // from https://kotlinlang.org/docs/reference/classes.html#constructors
    init {

        TheApp.graph().inject(this)

        loadStoreLocations()
    }

    private fun loadStoreLocations() {

        val hasNetworkAccess = HttpUtil.isNetworkAvailable(getApplication())
        if (!hasNetworkAccess) {
            // Toaster.debugShort(mActivity, "Wifi not found, DevApp  Unavailable ");
            Logr.debug("Wifi not found, DevApp Unavailable ")
            _isLoadingLD.value = false
            return
        }

        // Keep Rx subscription reference, in order for access to cancel later. See onCleared() below
        // Thanks to: https://medium.com/@CodyEngel/managing-disposables-in-rxjava-2-for-android-388722ae1e8a
        val disposable = mStoreLocationsRepository
                .loadStoreLocations()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { _isLoadingLD.value = false },
                        { throwable ->
                            _isLoadingLD.value = false
                            _errorLD.value = throwable
                        }
                )

        mDisposables.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()

        mDisposables.dispose()
    }

}
