package com.dev.demo2019.fragment_aac.viewmodel_aac

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dev.demo2019.model.mapper.AppMapper
import com.dev.demo2019.model.pojo.StoreLocation
import com.dev.demo2019.model.repository.StoreLocationsRepository
import com.dev.demo2019.platform.TheApp
import javax.inject.Inject

class StoreLocationsListViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var mRepository: StoreLocationsRepository
        @Inject set

    lateinit var mAppMapper: AppMapper
        @Inject set

    private val _itemsLiveData = MutableLiveData<List<StoreLocation>>()

    fun getItemsLiveData() = _itemsLiveData as LiveData<List<StoreLocation>>

    init {
        TheApp.graph().inject(this)

        _itemsLiveData.value = mRepository.getCachedStoreLocations()
    }

    fun getStoreLocationViewItem(position: Int) =
            mAppMapper.getViewModel(
                    mRepository.getCachedStoreLocation(position)
            )

}
