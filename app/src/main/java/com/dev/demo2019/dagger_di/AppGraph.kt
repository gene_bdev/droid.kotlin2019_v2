package com.dev.demo2019.dagger_di

import android.content.Context
import com.dev.demo2019.di.AppModule
import com.dev.demo2019.di.RepositoryModule
import com.dev.demo2019.fragment_aac.viewmodel_aac.StartActivityViewModel
import com.dev.demo2019.fragment_aac.viewmodel_aac.StoreDetailsViewModel
import com.dev.demo2019.fragment_aac.viewmodel_aac.StoreLocationsListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, RepositoryModule::class])
interface AppGraph {

    val context: Context

    fun inject(viewModel: StartActivityViewModel)

    fun inject(storeLocationsListViewModel: StoreLocationsListViewModel)
    fun inject(storeDetailsViewModel: StoreDetailsViewModel)
}
