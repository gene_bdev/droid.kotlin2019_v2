package com.dev.demo2019.di

import com.dev.demo2019.model.repository.AppState
import com.dev.demo2019.model.repository.StoreLocationsRepository
import com.dev.demo2019.platform.retrofit.AppRetrofitNetworkService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    internal fun provideAppRetrofitCalls(appState: AppState, appRetrofitNetworkService: AppRetrofitNetworkService) =
            StoreLocationsRepository(appState, appRetrofitNetworkService)

//    @Provides
//    @Singleton
//    internal fun provideTodoItemsRetrofitCalls(appState: AppState, appRetrofitNetworkService: AppRetrofitNetworkService) =
//             TodoItemsRepository(appState, appRetrofitNetworkService)


    @Provides
    @Singleton
    internal fun provideAppRetrofitNetworkService() = AppRetrofitNetworkService(STORES_URL)

    @Provides
    @Singleton
    internal fun provideAppState() = AppState()

    companion object {
        // private const val BASE_URL = "https://jsonplaceholder.typicode.com"
        private const val STORES_URL = "http://sandbox.bottlerocketapps.com"
    }

}
