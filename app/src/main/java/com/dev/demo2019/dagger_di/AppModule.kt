package com.dev.demo2019.di

import android.app.Application
import android.content.Context

import com.dev.demo2019.model.mapper.AppMapper

import javax.inject.Singleton

import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    internal fun provideContext() = application as Context

    @Provides
    @Singleton
    internal fun provideAppMapper(context: Context) = AppMapper(context)

}
