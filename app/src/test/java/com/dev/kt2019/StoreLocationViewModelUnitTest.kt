package com.dev.kt2019

import com.dev.demo2019.model.repository.StoreLocationsRepository
import com.dev.demo2019.model.viewmodel_pojo.StoreLocationViewModel
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertThat
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

/**
 */
class StoreLocationViewModelUnitTest {

    private val repository = mock(StoreLocationsRepository::class.java)

    private var storeLocationViewModel = StoreLocationViewModel(
            "Macy's",
            "555-1212",
            "W. 1st.",
            "San Jose",
            "95125",
            "CA"
    )


    @Test
    fun testNull() {
        assertThat(storeLocationViewModel.name, notNullValue())
        assertThat(storeLocationViewModel.city, notNullValue())

        verify(repository, never()).getCachedStoreLocations()
        verify(repository, never()).getCachedStoreLocation(ArgumentMatchers.anyInt())
    }


}
