package com.dev.kt2019

import com.dev.demo2019.model.pojo.StoreLocation
import com.dev.demo2019.model.pojo.StoresContainer
import com.dev.demo2019.model.repository.AppState
import com.dev.demo2019.model.repository.StoreLocationsRepository
import com.dev.demo2019.platform.retrofit.AppRetrofitEndpoints
import com.dev.demo2019.platform.retrofit.AppRetrofitNetworkService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class StoreLocationsRepositoryTest {

    // use these mockk<..>() to 1) create usable instance, 2) control what accessors will return, eg:
    // every { mAppState.getStoreLocation(mockPosition) } returns mStoreLocation
    private val mAppState = mockk<AppState>()
    private val mAppRetrofitNetworkService = mockk<AppRetrofitNetworkService>()

    // instance member that will be used in each test function
    // parameters for constructor will be generated via calls to : mockk<>() 
    private lateinit var mRepo: StoreLocationsRepository

    private lateinit var mStoreLocation: StoreLocation

    // https://mockk.io/#verification-atleast-atmost-or-exactly-times
    @Before
    fun setUp() {

        // call constructor with mockk-ed objects
        mRepo = StoreLocationsRepository(mAppState, mAppRetrofitNetworkService)

        mStoreLocation = StoreLocation(
                            "id123",
                            "Macy's",
                            "555-1212",
                            "http://abc.com/logo.jpg",
                            "W. 1st.",
                            "San Jose",
                            "95125",
                            "CA",
                            0,
                            0
                    )

    }

    @Test
    fun verify_getCachedStoreLocationTest() {

        // 1. mock up mAppState
        val mockPosition = 10
        every { mAppState.getStoreLocation(mockPosition) } returns mStoreLocation

        // https://blog.kotlin-academy.com/mocking-is-not-rocket-science-mockk-features-e5d55d735a98
        // 2. verify : In verify blocks you can check if that calls were performed
        val storeLocation = mRepo.getCachedStoreLocation(mockPosition)
        verify { mAppState.getStoreLocation(mockPosition) }

        Assert.assertEquals(storeLocation, mStoreLocation)
    }


    @Test
    fun verify_getStoreLocationsTest() {

        // use mockk<..>() to mock what this instance will return
        val mockApi = mockk<AppRetrofitEndpoints>()

        val mockStoresContainer = StoresContainer(listOf(mStoreLocation))

        // Create a synthetic successful response with body as the deserialized body
        // https://square.github.io/retrofit/2.x/retrofit/retrofit2/Response.html
        // from https://github.com/square/retrofit/issues/2089
        val mockRetrofitResponse = Response.success(mockStoresContainer)

        every { mAppRetrofitNetworkService.api } returns mockApi

        // from https://stackoverflow.com/a/52672277/2162226 :
        // Single.just(x) evaluates the x immediately in the current thread
        // and then you're left with whatever was the result of x, for all subscribers.
        // So here because we are testing `.assertSubscribed()`, we need the 2nd part : "and then you're left with whatever was the result of x, for all subscribers."
        every { mockApi.storeLocationsRx } returns Single.just(mockRetrofitResponse)

        // https://medium.com/@vanniktech/testing-rxjava-code-made-easy-4cc32450fc9a
        // .test() , and also introduces Single.just(..) structure
        mRepo
            .loadStoreLocations()
            .test()
            .assertSubscribed()

        verify { mAppState.storeLocations = mockStoresContainer.stores }
    }
}