# README #

This project is a Kotlin port for an equivalent pure .java implementation at:
https://bitbucket.org/gene_bdev/droid.demo2019/src/develop/

All changes in this project around around main module: /app 

### What is different here from droid.demo2019 Native Java implementation? ###

* Main /app module is pure Kotlin

* RxJava takes into account consideration for Disposable reference

* AAC (Android Architecture Components) pattern is applied
https://developer.android.com/topic/libraries/architecture

### JSON input

* As part of the demo here, the app makes a remote call using RxJava to get the json input.
- http://sandbox.bottlerocketapps.com/BR_Android_CodingExam_2015_Server/stores.json

* If that url becomes unavailable at some point, the JSON file is stored locally in the app at:
- /app/src/main/assets/json/stores.json

* Next Steps: Refactor project to allow for switching between remote and local stores.json file access. 

### Notes on AAC Implementation ###

As per Android dev articles from the last 12-18 months, best practices is moving away from MVP and now going toward AAC.

The main consideration I found in getting things set up with AAC is:

1. Set up AAC ViewModel objects that work with the main view class (fragment or activity)

2. Use `ViewModelProviders` reference to get a ViewModel instance

3. Maintain data via `LiveData` members

4. Set the AAC ViewModel listeners via :  `.observe()` ; When these are triggered in the view class, update the View accordingly   


### To list what is included here, below is a copy of features from pure .java repo ###

=====================
=====================
Architecture/Design Related Considerations:

1. Using /_commons lib module to distribute unnecessary complexity, out of the mainline /app module

2. Top level Java package structure is clean and well compartmentalized
    * *Main view packages (activity, fragment, and model)*  are at the top level
    * Then for all the other possible things we can add in, there is /platform to wrap that business-logic



=====================
=====================
Android libs/APIs:

1. Retrofit
    * Making 1 @GET call to public JSON at https://jsonplaceholder.typicode.com/todos

2. RxJava with Retrofit
    * Hooked in a basic RxJava setup with the existing retrofit configuation

3. Dagger2
    * Several Java objects can now be added via `@Inject`

4. Gson
    * Mapping/"marshalling" json strings into POJO java objects without a bunch of `get/set` calls



=====================
=====================
Handy/Powerful Utils:

1. `package com.dev.commons.core.fragment_accessor;`
    * Provides a clean solution to:
    
        * Wrap common multi-line calls to FragmentManager (eg, .replaceFragment())
    
        * Manage double back-click to exit a fragment container activity


