package com.dev.commons.core.fragment_accessor.app_fragment;


import com.dev.commons.core.fragment_accessor.RawFragmentAccessor;

/**
 * Wrapper to be used by project module
 * that will be passing in subclass instances of BaseAppFragment
 *
 * Super class RawFragmentAccessor represents a purely generic implementation
 * for passing in SDK Fragment objects
 *  - no BaseAppFragment .. aka, implementation-level code
 */
public class AppFragmentAccessor extends RawFragmentAccessor {

    public void replaceFragment(RawBaseFragment rawBaseFragment) {

        replaceFragment(
                rawBaseFragment,
                rawBaseFragment.getFragmentTag(),
                rawBaseFragment.getFragmentTag());
    }

    public void addFragment(RawBaseFragment rawBaseFragment) {

        addFragment(
                rawBaseFragment,
                rawBaseFragment.getFragmentTag());
    }



}
