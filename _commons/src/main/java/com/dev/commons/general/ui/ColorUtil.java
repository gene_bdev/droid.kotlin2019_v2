package com.dev.commons.general.ui;

import android.content.Context;

import androidx.core.content.ContextCompat;

public class ColorUtil {

    public static int getResolvedColorResource(Context context, int colorResId) {

        int resolvedColorResource = ContextCompat.getColor(context, colorResId);
        return resolvedColorResource;

    }
}
