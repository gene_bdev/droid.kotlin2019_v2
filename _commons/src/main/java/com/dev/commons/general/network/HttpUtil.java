package com.dev.commons.general.network;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.dev.commons.general.ui.DialogUtil;

import java.io.InputStream;
import java.net.URL;


public class HttpUtil {

    public interface NetworkCheckListener {
        void onNetworkConnected();
    }

    /**
     * Simple helper for checking network connectivity
     * Requires that manifest has ACCESS_NETWORK_STATE permission
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }

        return false;
    }

    public static boolean runNetworkCheckWithDialog(Activity activity) {

        boolean hasNetworkAccess = HttpUtil.isNetworkAvailable(activity.getApplicationContext());
        if (! hasNetworkAccess) {

            DialogUtil
                    .getErrorDialog(
                            activity,
                            "No Network Access, please try again when you have access"
                    )
                    .show();
        }

        return hasNetworkAccess;
    }

    public static Drawable getUrlImageAsDrawable(String url) {

        try {

            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        }
        catch (Exception ex) {

            ex.printStackTrace();
            return null;
        }
    }

    public static Bitmap getUrlImageAsBitmap(String url) {

        try {

            URL urlRef = new URL(url);
            Bitmap urlImageAsBitmap = BitmapFactory.decodeStream(urlRef.openConnection().getInputStream());

            return urlImageAsBitmap;
        }
        catch (Exception ex) {

            ex.printStackTrace();
            return null;
        }
    }

}
