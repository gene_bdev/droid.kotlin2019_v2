package com.dev.commons.general.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;


public class DialogUtil {

    public static AlertDialog getErrorDialog(Activity activity, String errorMessage) {
        return getErrorDialog(activity, null, errorMessage);
    }

    public static AlertDialog getErrorDialog(Activity activity, String title, String errorMessage) {
        return getInfoDialog(activity, title, errorMessage);
    }

    public static AlertDialog getInfoDialog(Activity activity, String title, String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setCancelable(true);
        if (title != null) {
            alertDialog.setTitle(title);
        }
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        return alertDialog;
    }

    public static void showNoNetworkStartUpErrorDialog(final Activity activity,
                                                       DialogInterface.OnClickListener onOfflineClickListener) {

        String title = "Networking is offline";
        String errorMessage = "Please get online to use app";

        boolean isDevMode = true;
        if (isDevMode) {
            errorMessage = "DEV ONLY: \n Using Mock Data";
        }

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
        alertDialog.setCancelable(true);
        alertDialog.setTitle(title);
        alertDialog.setMessage(errorMessage);

        alertDialog.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.setPositiveButton("Use Offline", onOfflineClickListener);
        alertDialog.show();
    }

    public static AlertDialog getCustomDialog(Activity activity, String title, View layoutView) {

        return DialogUtil.getCustomDialog(- 1, activity, title, layoutView);
    }

    public static AlertDialog getCustomDialog(int dialogStyleResId, Activity activity, String title, View layoutView) {

        AlertDialog alertDialog = (dialogStyleResId > 0)
                ? new AlertDialog.Builder(activity, dialogStyleResId).create()
                : new AlertDialog.Builder(activity).create();

        alertDialog.setCancelable(true);
        if (title != null) {
            alertDialog.setTitle(title);
        }

        alertDialog.setView(layoutView);

        // Let caller set these
        // alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Close",
        //         new DialogInterface.OnClickListener() {
        //             public void onClick(DialogInterface dialog, int which) {
        //                 dialog.dismiss();
        //             }
        //         });

        return alertDialog;
    }


    public static void setAppDialogStyle(AlertDialog alertDialog) {

        if (! alertDialog.isShowing()) {

            Toaster.unexpectedError(alertDialog.getContext(), "Can't set style until dialog is shown");
            return;
        }

        alertDialog
                .getWindow()
                .setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);

        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
    }

}


        /*
         * To do after .show()
         *  - set colors
         *  - set listeners
         *
        Resources dialogResources = alertDialog.getContext().getResources();
        int transparentColor = dialogResources.getColor(R.color.transparent);
        int textColor = dialogResources.getColor(R.color.dark_gray);

        int dividerId = alertDialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
        View titleDivider = alertDialog.findViewById(dividerId);
        titleDivider.setBackgroundColor(transparentColor);

        int textViewId = alertDialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
        TextView tv = alertDialog.findViewById(textViewId);
        tv.setTextColor(textColor);
        */
