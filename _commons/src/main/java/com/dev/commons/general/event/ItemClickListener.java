package com.dev.commons.general.event;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(View view, int position);
}
