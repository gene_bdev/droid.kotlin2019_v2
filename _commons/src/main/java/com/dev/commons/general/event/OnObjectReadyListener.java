package com.dev.commons.general.event;

public interface OnObjectReadyListener {

    // cast this the same we do w/retrofit
    void onReady(Object object);
}
