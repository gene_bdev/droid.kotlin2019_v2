package com.dev.commons.general.event;

public interface OnReadyListener {

    void onReady();
}
