package com.dev.commons.general.util;

import android.app.AlertDialog;

public class CCommon {

    public static class Default {
        public final static int INVALID_INDEX = -1;
        public final static boolean CLOSE_KEYBOARD_ON_SEND = true;
    }

    public class Size {

        public final static int THUMBNAIL_SCALE = 200;
        public final static int SMALL_MEDIUM = 400;
        public final static int PDF_REPORT_SCALE = 250;

        public static final int DEFAULT_MSG_LENGTH_LIMIT = 200;
    }

    public static class Result {

        public final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 800;
        public final static int GET_DEMO_TOKEN_REQUEST_CODE = 801;
    }

    public static class Field {

        public final static String id = "id";
        public final static String key = "key";
        public final static String email = "email";
        public final static String type = "type";
        public final static String role = "role";
        public final static String phone = "phone";
        public final static String date = "date";
        public final static String time = "time";
        public final static String details = "details";
        public final static String address = "address";
        public final static String userName = "userName";
        public final static String firstName = "firstName";
        public final static String lastName = "lastName";
        public final static String photoUrl = "photoUrl";
        public final static String imgUrl = "imgUrl";
        public final static String title = "title";
        public final static String appTitle = "_title";
        public final static String appProfile = "appProfile";
        public final static String arrayIdx = "arrayIdx";

        public final static String createTime = "createTime";
        public final static String uid = "uid";

        // public final static String tag = "tag";
        public final static String tagId = "tagId";
        public final static String text = "text";
        public final static String postTime = "postTime";
        public final static String updatedOn = "updatedOn";
        public final static String payload = "payload";
        public final static String message = "message";
        public final static String error = "error";
        public final static String status = "status";
        public final static String httpStatus = "httpStatus"; // 200, etc

    }

    public static class Dialog {

        public final static int CENTER_BUTTON = AlertDialog.BUTTON_NEUTRAL;
        public final static int LEFT_BUTTON = AlertDialog.BUTTON_NEGATIVE;
        public final static int RIGHT_BUTTON = AlertDialog.BUTTON_POSITIVE;
    }

}
